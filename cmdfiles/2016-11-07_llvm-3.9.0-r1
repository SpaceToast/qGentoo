# vim: set ft=sh:
die() {
	printf "die called, dropping a shell\n"
	bash
}

ensure_dir() {
	local dir=$1
	if [[ ! -d $dir ]]; then
		if [[ -e $dir ]]; then
			mv "$dir" "${dir}.old" || die
		fi
		mkdir -p "$dir" || die
		if [[ -e ${dir}.old ]]; then
			mv "${dir}.old" "${dir}/old" || die
		fi
	fi
}

add_p_a_k_2() {
	if [[ $# -ne 2 ]]; then
		echo "$FUNCTION: Wrong number of args ( $#, expected 2 )"
		die
	fi
	ensure_dir "/etc/portage/package.accept_keywords"
	printf "%s %s\n" "$1" "$2" >> "/etc/portage/package.accept_keywords/p_a_k_2"
}

add_p_a_k() {
	ensure_dir "/etc/portage/package.accept_keywords"
	for atom; do
		printf "%s\n" "$atom" >> "/etc/portage/package.accept_keywords/p_a_k"
	done
}

add_p_unmask() {
	local dir="/etc/portage/package.unmask"
	ensure_dir "$dir"
	for atom; do
		printf "%s\n" "$atom" >> "$dir"/p_unmask
	done
}

add_p_use() {
	ensure_dir /etc/portage/package.use
	local atom=$1
	shift
	printf "%s %s\n" "$atom" "$*" >> /etc/portage/package.use/p_use
}

add_p_use2() {
	ensure_dir /etc/portage/package.use
	local flag=$1
	shift
	local pkg
	for pkg; do
		printf "%s %s\n" "$pkg" "$flag" >> /etc/portage/package.use/p_use
	done
}

set_use_expand() {
	set_portage_var "$@"
}

set_portage_var() {
	local v=$1
	shift
	printf "%s=\"%s\"\n" "$v" "$*" >> /etc/portage/make.conf || die
}

prep_virtualx() {
	add_p_use "x11-base/xorg-server" "xvfb" "-xorg" "minimal"
	add_p_use "media-libs/mesa" "-llvm"
	set_use_expand VIDEO_CARDS
}

add_feature() {
	printf "FEATURES=\"\$FEATURES %s\"\n" "$*" >> /etc/portage/make.conf || die
}

add_p_feature() {
	local atom=$1
	local feature=$2
	mkdir -p "/etc/portage/env" || die
	printf "FEATURES=\"%s\"\n" "$feature" >> "/etc/portage/env/p_feature_${feature}.conf" || die

	ensure_dir "/etc/portage/package.env"
	printf "%s %s\n" "$atom" "p_feature_${feature}.conf" >> /etc/portage/package.env/p_feature || die
}

add_p_feature2() {
	local feature=$1
	shift
	mkdir -p "/etc/portage/env" || die
	printf "FEATURES=\"%s\"\n" "$feature" >> "/etc/portage/env/p_feature_${feature}.conf" || die
	ensure_dir "/etc/portage/package.env"

	local atom
	for atom; do
		 printf "%s %s\n" "$atom" "p_feature_${feature}.conf" >> /etc/portage/package.env/p_feature || die
	done
}

enable_test() {
	for p; do
		add_p_feature "$p" "test"
	done
}

add_use_stable_mask() {
	ensure_dir "/etc/portage/profile"
	printf "%s\n" "$@" >> /etc/portage/profile/use.stable.mask || die
}

prep_python35() {
	set_portage_var PYTHON_TARGETS python2_7 python3_4 python3_5
	add_use_stable_mask "-python_targets_python3_5"
	add_p_a_k "=dev-lang/python-3.5*"
}

prep_gcc() {
	local gccver=$1
	add_p_a_k_2 "~sys-devel/gcc-$gccver" "**"
	emerge --quiet "~sys-devel/gcc-$gccver" || die
	gcc-config "x86_64-pc-linux-gnu-$gccver" || die
	. /etc/profile || die

	emerge --quiet "<app-portage/gentoolkit-0.3.1" || die
	revdep-rebuild --library 'libstdc\+\+\.so\.6' -- --exclude gcc --quiet || die
	# emerge --quiet ">=app-portage/gentoolkit-0.3.1" || die
	# revdep-rebuild --library 'libstdc++.so.6' -- --exclude gcc --quiet || die
}

prep_layman() {
	add_p_use "dev-vcs/git" "-perl" "-gpg"
	emerge --quiet --jobs=2 layman || die
	layman --quiet -S || die
	printf "source %s\n" "/var/lib/layman/make.conf" >> /etc/portage/make.conf
}

swapfile() {
	# 10G file
	dd if=/dev/zero of=/swap bs=1M count=10k || die
	mkswap /swap || die
	swapon /swap || die
}

swapfile
set_portage_var "PORTAGE_ELOG_CLASSES" "log warn error qa"
set_portage_var "MAKEOPTS" ""
set_use_expand "CPU_FLAGS_X86" ""

p=(
=sys-devel/llvm-3.9.0-r1
)
add_p_a_k "${p[@]}"
add_p_unmask "${p[@]}"

add_p_use sys-devel/llvm clang
# add_p_use sys-devel/llvm default-compiler-rt default-libcxx gold xml
add_p_a_k =sys-devel/clang-runtime-3.9.0 =sys-devel/clang-3.9.0-r100 =sys-libs/libomp-3.9.0 =dev-util/cmake-3.6.3

#prep_layman
#layman --quiet -a science || die
# prep_python35
# prep_virtualx
# prep_gcc "5.4.0"
# prep_gcc "6.2.0"

# emerge --quiet tmux || die

emerge --quiet --jobs=2 --oneshot --onlydeps "${p[@]}" || die
add_p_feature2 "test" "${p[@]}"
#add_p_feature "$p" "ipc-sandbox"
#add_p_feature "$p" "network-sandbox"
emerge --quiet --jobs=2 --oneshot --onlydeps "${p[@]}" || die
#set_portage_var "MAKEOPTS" "-j4"
emerge --verbose "${p[@]}" || die
# tmux new-session emerge --verbose --oneshot "$p"
# CMAKE_VERBOSE=yes TEST_VERBOSE=yes emerge --verbose --oneshot "$p" || die
