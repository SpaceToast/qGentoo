#!/usr/bin/env python3
# By eroen, 2016

from __future__ import print_function

import argparse
import os
import shutil
import subprocess
import sys
import tempfile

# IMGSIZE = int(3.2 * 2 ** 30)
IMGSIZE = 10 * 2 ** 30
# IMGSIZE = 20 * 2 ** 30
CACHEDIR = '/var/cache/freshstage3'


def pause():
    print('Press ^d to continue', file=sys.stderr)
    sys.stderr.flush()
    sys.stdin.read()


def makeimagefile(tdir, s):
    fn = os.path.join(tdir, 'image')
    f = open(fn, 'bw')
    f.seek(s - 1)
    f.write(b'\0')
    f.close()
    return fn


def partitionimagefile(fn):
    subprocess.call(['sgdisk',
                     '--largest-new=1',
                     fn])


def mkfs(fn):
    subprocess.call(['mke2fs', '-F',
                     '-t', 'ext4',
                     '-T', 'news',
                     fn])


def launchqemu(fn, kernel, initramfs=None, cmdline=''):
    cmdline += ' noefi console=ttyS0,115200'
    args = ['qemu-system-x86_64',
            '-cpu', 'host',
            '-smp', 'cpus=4,cores=2,threads=2,maxcpus=4',
            # '-m', 'size=2048',
            '-m', 'size=4096',
            # '-m', 'size=6144',
            # '-display', 'curses', '-serial', 'stdio',
            '-nographic', '-enable-kvm',
            '-kernel', kernel,
            '-append', cmdline,
            '-initrd', initramfs]

    # usb passthrough
    usbdevs = [(1, 1.6)]  # UVC camera
    args.extend(['-device', 'usb-ehci,id=usb'])
    # args.append('-usb')
    for ud in usbdevs:
        args.extend(['-device',
                     'usb-host,bus=usb.0,hostbus={},hostport={}'.format(*ud)])

    # image filename
    args.append(fn)

    subprocess.call(args)


def updateimage(root, script, hardened=False):
    # Update fstab

    # Set timezone
    os.remove(os.path.join(root, 'etc/localtime'))
    os.symlink('/usr/share/zoneinfo/UTC', os.path.join(root, 'etc/localtime'))

    # Set profile
    os.remove(os.path.join(root, 'etc/portage/make.profile'))
    if hardened:
        profile = 'hardened/linux/amd64'
    else:
        profile = 'default/linux/amd64/13.0'
    os.symlink(os.path.join('/usr/portage/profiles', profile),
               os.path.join(root, 'etc/make.profile'))

    # Autostart network service
    netdev = 'ens3'
    os.symlink(
        'net.lo',
        os.path.join(root, 'etc/init.d/net.{}'.format(netdev)))
    os.symlink(
        '/etc/init.d/net.{}'.format(netdev),
        os.path.join(root, 'etc/runlevels/default/net.{}'.format(netdev)))

    # Start a getty on ttyS0, autologin root
    with open(os.path.join(root, 'etc/inittab'), 'a') as f:
        f.write('\n')
        f.write(
            's0:12345:respawn:/sbin/agetty -a root -L 115200 ttyS0 xterm\n')

    # Run script on root login
    with open(os.path.join(root, 'root/.bash_login'), 'a') as f:
        # f.write('printf "%s\\n" "This is ~/.bash_login"\n')
        # f.write('emerge --info\n')
        f.write('bash /testscript.bash\n')
    shutil.copyfile(script, os.path.join(root, 'testscript.bash'))


class FileCheckError(Exception):
    pass


def getfile(uri, name=None):
    import requests
    if name is None:
        name = os.path.basename(uri)

    filename = os.path.join(CACHEDIR, name)
    print('Fetching {uri} '.format(uri=uri), end='', file=sys.stderr)
    sys.stderr.flush()
    r = requests.get(uri, stream=True)
    with open(filename, 'wb') as f:
        for chunk in r.iter_content(2**20):
            f.write(chunk)

            print('.', end='', file=sys.stderr)
            sys.stderr.flush()

    print(file=sys.stderr)
    sys.stderr.flush()


def delfile(uri, name=None):
    if name is None:
        name = os.path.basename(uri)

    filename = os.path.join(CACHEDIR, name)
    print('Deleting {uri} '.format(uri=uri), file=sys.stderr)
    sys.stderr.flush()
    os.unlink(filename)


def checkfile(name, size=None):
    path = os.path.join(CACHEDIR, name)
    # if not os.path.exists(path):
    #     raise os.FileNotFoundError
    s = os.stat(path)
    asize = s.st_size
    if size is not None and \
       int(asize) != int(size):
        raise FileCheckError(
            'Actual size {a} differs from expected size {e}'.format(
                a=asize, e=size))


def getstage3(hardened=False):
    import requests
    uribase = 'http://distfiles.gentoo.org/releases/amd64/autobuilds/'
    if hardened:
        latest = 'latest-stage3-amd64-hardened.txt'
    else:
        latest = 'latest-stage3-amd64.txt'
    r = requests.get(os.path.join(uribase, latest))

    for l in r.text.splitlines():
        if l.startswith('#'):
            pass
        else:
            [relpath, exsize] = l.split(' ')
            print(relpath, exsize)
            try:
                checkfile(os.path.basename(relpath), size=int(exsize))
            except OSError as e:
                print(e, file=sys.stderr)
                getfile(os.path.join(uribase, relpath))
                checkfile(os.path.basename(relpath), size=int(exsize))

            return(os.path.basename(relpath))


def getportagesnapshot():
    latest = 'http://distfiles.gentoo.org/snapshots/portage-latest.tar.xz'
    name = os.path.basename(latest)

    import requests
    r = requests.get(latest, stream=True)
    contentlength = int(r.headers['Content-Length'])
    lastmodified = r.headers['Last-Modified']
    r.close()

    print('{f} {s} {d}'.format(f=name,
                               s=contentlength,
                               d=lastmodified))
    try:
        checkfile(name, size=contentlength)
    except OSError as e:
        print(e, file=sys.stderr)
        getfile(latest)
        checkfile(name, size=contentlength)
    except FileCheckError as e:
        print(e, file=sys.stderr)
        delfile(latest)
        getfile(latest)
        checkfile(name, size=contentlength)

    return(name)


def main():
    parser = argparse.ArgumentParser(
        description='Execute a script in a fresh qemu VM.')
    parser.add_argument('--hardened', action='store_true',
                        help='use a hardened stage3')
    parser.add_argument('--hardened_kernel', action='store_true',
                        help='use a hardened kernel')
    parser.add_argument('--tmpdir', action='store', default=None, 
            help='base directory for temporary files')
    parser.add_argument('--image', action='store', default=None, 
            help='existing file or device used for the qemu disk image')
    parser.add_argument('script', action='store', help='script to run')
    args = parser.parse_args()
    print('Hardened? ', args.hardened, file=sys.stderr)
    print('Hardened kernel? ', args.hardened_kernel, file=sys.stderr)
    print('Script:   ', args.script, file=sys.stderr)

    tdir = tempfile.mkdtemp(prefix='freshstage3_', dir=args.tmpdir)
    print(tdir, file=sys.stderr)

    # kernel, initramfs
    if args.hardened_kernel:
        # kver = '4.1.7-hardened-r1'
        kver = '4.4.8-hardened-r1'
    else:
        # kver = '4.1.15-gentoo-r1'
        kver = '4.4.6-gentoo'

    subprocess.call(['tar', 'xp',
                     '-C', tdir,
                     '-f', os.path.join(CACHEDIR,
                                        'linux-{}-kern.tar.gz'.format(kver))])

    kernel = os.path.join(
        tdir, 'kernel-genkernel-x86_64-{kver}'.format(kver=kver))
    initramfs = os.path.join(
        tdir, 'initramfs-genkernel-x86_64-{kver}'.format(kver=kver))

    # disk image:
    if args.image is not None:
        ifn = args.image
    else:
        ifn = makeimagefile(tdir, IMGSIZE)
    # partitionimagefile(ifn)
    mkfs(ifn)
    # - get stage3 + portage snapshot
    #   - http://distfiles.gentoo.org/releases/amd64/autobuilds/
    #     current-stage3-amd64/stage3-amd64-20151119.tar.bz2
    # - mount
    os.mkdir(os.path.join(tdir, 'mnt'))
    subprocess.call(['mount',
                     '-o', 'loop,acl,user_xattr',
                     '-t', 'ext4',
                     ifn,
                     os.path.join(tdir, 'mnt')])

    try:
        # - get, unpack stage3
        stage3 = getstage3(hardened=args.hardened)
        subprocess.call(['tar',
                         'xpf', os.path.join(CACHEDIR, stage3),
                         '--numeric-owner', '--xattrs', '--acls',
                         '-C', os.path.join(tdir, 'mnt')])
        # - unpack portage snapshot
        # snapshot = 'portage-latest.tar.xz'
        snapshot = getportagesnapshot()
        subprocess.call(['tar',
                         'xpf', os.path.join(CACHEDIR, snapshot),
                         '--numeric-owner', '--xattrs', '--acls',
                         '-C', os.path.join(tdir, 'mnt/usr')])

        # - install modules
        subprocess.call(['tar', 'xp',
                         '--keep-directory-symlink',  # preserve lib -> lib64
                         '-C', os.path.join(tdir, 'mnt'), '-f',
                         os.path.join(CACHEDIR,
                                      'linux-{}-modules.tar.gz'.format(kver))])

        # - make changes
        updateimage(os.path.join(tdir, 'mnt'), script=args.script,
                    hardened=args.hardened)

        # - unmount, sync & c.
        subprocess.call(['sync'])
    finally:
        subprocess.call(['umount', os.path.join(tdir, 'mnt')])

    launchqemu(ifn, kernel, initramfs, 'root=/dev/sda rootflags=discard')
    return 0


if __name__ == '__main__':
    exit(main())
